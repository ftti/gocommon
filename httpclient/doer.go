package httpclient

import (
	"io/ioutil"
	"net/http"
	"time"
)

type HttpDoer interface {
	Do(req *http.Request) ([]byte, error)
	DoRawResponse(req *http.Request) (*http.Response, error)
}

type httpDoer struct {
	client *http.Client
}

func NewHttpDo(timeout time.Duration) HttpDoer {
	client := &http.Client{
		Timeout: timeout,
	}
	return newDoer(client)
}

func newDoer(client *http.Client) HttpDoer {
	return &httpDoer{client: client}
}

func (d *httpDoer) Do(req *http.Request) ([]byte, error) {
	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return respBytes, err
}

func (d *httpDoer) DoRawResponse(req *http.Request) (resp *http.Response, err error) {
	resp, err = d.client.Do(req)

	return
}
